/*jshint maxerr: 10000000 */

// global variable
var xp = 0;
var level = 1;
const demo = document.getElementById("demo");
const adc = document.getElementById("adc");
const id_1 = document.getElementById("id_1");
const button = document.getElementById("button");

config();
button.addEventListener("click",GameFunction);

function config() {
    demo.innerHTML = "0 XP";
    adc.innerHTML = "Level: 1";
    id_1.innerHTML = "Você precisa de 400 XP para passar de nível";
    button.innerHTML = "XP";
}

// events
function GameFunction() {
    xp += 30;
    const game_1 = "Você precisa de ";
    const game_2 = " XP para passar de nível";
    var game_3 = Level();
    const game_4 = game_1 + (game_3 - xp) + game_2;
    demo.innerHTML = Separator(xp," ") + " XP";
    adc.innerHTML = "Level: " + level;
    id_1.innerHTML = game_4;
}

// Level function
function Level() {
    if (xp < 400) {
        level = 1;
        return 400;
    }
    if (xp >= 400 && xp < 1200) {
        level = 2;
        return 1200;
    }
    if (xp >= 1200 && xp < 2400) {
        level = 3;
        return 2400;
    }
    if (xp >= 2400 && xp < 4010) {
        level = 4;
        return 4010;
    }
    if (xp >= 4010 && xp < 6030) {
        level = 5;
        return 6030;
    }
    if (xp >= 6030 && xp < 9060) {
        level = 6;
        return 9060;
    }
    if (xp >= 9060 && xp < 12600) {
        level = 7;
        return 12600;
    }
    if (xp >= 12600 && xp < 16660) {
        level = 8;
        return 16660;
    }
    if (xp >= 16660 && xp < 21240) {
        level = 9;
        return 21240;
    }
    if (xp >= 21240 && xp < 26340) {
        level = 10;
        return 26340;
    }
    if (xp >= 26340 && xp < 37460) {
        level = 11;
        return 37460;
    }
    if (xp >= 37460 && xp < 49600) {
        level = 12;
        return 49600;
    }
    if (xp >= 49600 && xp < 62760) {
        level = 13;
        return 62760;
    }
    if (xp >= 62760 && xp < 76950) {
        level = 14;
        return 76950;
    }
    if (xp >= 76950 && xp < 92170) {
        level = 15;
        return 92170;
    }
    if (xp >= 92170 && xp < 108420) {
        level = 16;
        return 108420;
    }
    if (xp >= 108420 && xp < 125700) {
        level = 17;
        return 125700;
    }
    if (xp >= 125700 && xp < 144020) {
        level = 18;
        return 144020;
    }
    if (xp >= 144020 && xp < 163380) {
        level = 19;
        return 163380;
    }
    if (xp >= 163380 && xp < 183780) {
        level = 20;
        return 183780;
    }
    if (xp >= 183780 && xp < 218010) {
        level = 21;
        return 218010;
    }
    if (xp >= 218010 && xp < 254530) {
        level = 22;
        return 254530;
    }
    if (xp >= 254530 && xp < 293400) {
        level = 23;
        return 293400;
    }
    if (xp >= 293400 && xp < 334680) {
        level = 24;
        return 334680;
    }
    if (xp >= 334680 && xp < 378430) {
        level = 25;
        return 378430;
    }
    if (xp >= 378430 && xp < 424710) {
        level = 26;
        return 424710;
    }
    if (xp >= 424710 && xp < 473580) {
        level = 27;
        return 473580;
    }
    if (xp >= 473580 && xp < 525100) {
        level = 28;
        return 525100;
    }
    if (xp >= 525100 && xp < 579330) {
        level = 29;
        return 579330;
    }
    if (xp >= 579330 && xp < 636330) {
        level = 30;
        return 636330;
    }
    if (xp >= 636330 && xp < 1014530) {
        level = 31;
        return 1014530;
    }
    if (xp >= 1014530 && xp < 1411330) {
        level = 32;
        return 1411330;
    }
    if (xp >= 1411330 && xp < 1827130) {
        level = 33;
        return 1827130;
    }
    if (xp >= 1827130 && xp < 2262330) {
        level = 34;
        return 2262330;
    }
    if (xp >= 2262330 && xp < 2717330) {
        level = 35;
        return 2717330;
    }
    if (xp >= 2717330 && xp < 3192530) {
        level = 36;
        return 3192530;
    }
    if (xp >= 3192530 && xp < 3688330) {
        level = 37;
        return 3688330;
    }
    if (xp >= 3688330 && xp < 4205130) {
        level = 38;
        return 4205130;
    }
    if (xp >= 4205130 && xp < 4743330) {
        level = 39;
        return 4743330;
    }
    if (xp >= 4743330 && xp < 5303330) {
        level = 40;
        return 5303330;
    }
    if (xp >= 5303330 && xp < 5885530) {
        level = 41;
        return 5885530;
    }
    if (xp >= 5885530 && xp < 6490330) {
        level = 42;
        return 6490330;
    }
    if (xp >= 6490330 && xp < 7118130) {
        level = 43;
        return 7118130;
    }
    if (xp >= 7118130 && xp < 7769330) {
        level = 44;
        return 7769330;
    }
    if (xp >= 7769330) {
        level = 45;
        return xp;
    }
}
function Separator(a,sep) {
    if (a < 1000) {
        return a;
    }
    if (a > 999 && a < 1000000) {
        let b = String(a);
        let f1 = b.slice(0,b.length-3)+sep+b.slice(b.length -3,b.length)
        return f1;
    }
    if (a > 999999 && a < 1000000000) {
        let b = String(a);
        let f1 = b.slice(0,b.length-6)+sep+b.slice(b.length-6,b.length-3)+sep+b.slice(b.length-3,b.length);
        return f1;
    }
}