/*jshint maxerr: 10000000 */
// Global variable
var score = 0;
var highscore = {};
var num = 0;

// scene start
function startGame() {
    Objects.fundo = new Rect(40,40,"black",20,20);
    Objects.fundo2 = new Rect(40,40,"yellow",360,Math.floor(Math.random()*230));
    Objects.fundo1 = new Rect(40,40,"yellow",360,Math.floor(Math.random()*230));
    Objects.myText = new Text("15px Arial","blue","Score: 0",20,30);
    Objects.myText2 = new Text("10px Calibri","red","Highscore: 0",20,50);
    myGameArea.start();
}

var myGameArea = {
    canvas : document.createElement("canvas"),
    textarea: document.createElement("input"),
    start : function() {
        this.canvas.width = 400;
        this.canvas.height = 270;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.textarea.addEventListener("keypress", move);
        this.textarea.type = "number";
        this.textarea.style.width = "10px";
        this.textarea.style.height = "10px";
        this.textarea.style.fontSize = "1px";
        document.body.insertBefore(this.textarea,document.body.childNodes[0]);
        this.interval = setInterval(updateGameArea, 20);
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
};

function updateGameArea() {
    myGameArea.clear();
    if (Objects.fundo2.x >= Objects.fundo.x-40 && Objects.fundo2.x <= Objects.fundo.x+40 && Objects.fundo2.y >= Objects.fundo.y-40 && Objects.fundo2.y <= Objects.fundo.y+40) {
        highscore["score"+num] = score;
        Objects.myText2.txt = "Highscore: " + Rank_Tools.rank(highscore,(pos,name,value) => {
            return value;
        }).rank1;
        num += 1;
        score = 0;
        Objects.fundo.x = 20;
        Objects.fundo.y = 20;
        Objects.fundo1.x = 360;
        Objects.fundo.y = Math.floor(Math.random()*230);
        Objects.fundo2.x = 360;
        Objects.fundo2.y = Math.floor(Math.random()*230);
    } else if (Objects.fundo1.x >= Objects.fundo.x-40 && Objects.fundo1.x <= Objects.fundo.x+40 && Objects.fundo1.y >= Objects.fundo.y-40 && Objects.fundo1.y <= Objects.fundo.y+40) {
        highscore["score"+num] =  score;
        Objects.myText2.txt = "Highscore: " + Rank_Tools.rank(highscore,(pos,name,value) => {
            return value;
        }).rank1;
        num += 1;
        score = 0;
        Objects.fundo.x = 20;
        Objects.fundo.y = 20;
        Objects.fundo1.x = 360;
        Objects.fundo1.y = Math.floor(Math.random()*230);
        Objects.fundo2.x = 360;
        Objects.fundo2.y = Math.floor(Math.random()*230);
    } else {
        score += 10;
    }
    for (let f0 in Objects) {
        Objects[f0].update();
    }
    Objects.myText.txt = "Score: "+score;
    if (Objects.fundo2.x >= 0-40) {
        Objects.fundo2.x -= (1/20)*score/200;
    } else {
        Objects.fundo2.x = 360;
        Objects.fundo2.y = Math.floor(Math.random()*230);
    }
    if (Objects.fundo1.x >= 0-40) {
        Objects.fundo1.x -= (1/20)*score/200;
    } else {
        Objects.fundo1.x = 360;
        Objects.fundo1.y = Math.floor(Math.random()*230);
    }
}
function move(event) {
    if (Objects.fundo.y > -20 && Objects.fundo.y < 290) {
        if (event.key == "2") {
            Objects.fundo.y -= 10;
        }
        if (event.key == "8") {
            Objects.fundo.y += 10;
        }
    } else {
        Objects.fundo.y = 0;
    }
    myGameArea.textarea.value = "";
}