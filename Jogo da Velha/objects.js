﻿// objects
const Objects = {};

function Rect(width, height, color, x, y) {
    this.width = width;
    this.height = height;
    this.color = color;
    this.x = x;
    this.y = y;
    this.update = function() {
        ctx = myGameArea.context;
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
    };
}

function Text(font,color,txt,x,y) {
    this.font = font;
    this.color = color;
    this.txt = txt;
    this.x = x;
    this.y = y;
    this.update = function() {
        ctx = myGameArea.context;
        ctx.font = this.font;
        ctx.fillStyle = this.color;
        ctx.fillText(this.txt,this.x,this.y);
    };
}
