﻿var myGameArea = {
    canvas : document.createElement("canvas"),
    button : document.createElement("button"),
    start : function() {
        this.canvas.width = 400;
        this.canvas.height = 270;
        this.context = this.canvas.getContext("2d");
        this.canvas.addEventListener("click", p);
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.button.addEventListener("click",reset);
        this.button.innerHTML = "Reset";
        document.body.insertBefore(this.button,document.body.childNodes[1]);
        this.interval = setInterval(updateGameArea, 20);
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
};
