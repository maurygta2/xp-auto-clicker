/*jshint maxerr: 10000000 */
// Global variable
var game_over = false;
var player = Math.floor(Math.random()*2)+1;
var reset_game = false;

// scene start
function startGame() {
    Objects.plano = new Rect(180,1,"black",110,111);
    Objects.plano2 = new Rect(180,1,"black",110,172);
    Objects.plano3 = new Rect(1,180,"black",171,50);
    Objects.plano4 = new Rect(1,180,'black',232,50);
    if (player === 1) {
        Objects.myText = new Text("10px Arial","black","Sua vez blue",0,15);
    } else {
        Objects.myText = new Text("10px Arial","black","Sua vez red",0,15);
    }
    myGameArea.start();
}

function updateGameArea() {
    myGameArea.clear();
    if (!reset_game) {
        for (let f0 in Objects) {
        Objects[f0].update();
        }
    } else {
        for (let f1 in Objects) {
            if (Objects[f1].color == "red" || Objects[f1].color == "blue") {
                delete Objects[f1];
            }
        }
        player = Math.floor(Math.random()*2)+1;
        game_over = false;
        if (player === 1) {
            Objects.myText.txt = "Sua vez blue";
        } else {
            Objects.myText.txt = "Sua vez red";
        }
        reset_game = false;
    }
    if (player === 1 && !game_over) {
        Objects.myText.txt = "Sua vez blue";
    }
    if (player === 2 && !game_over) {
        Objects.myText.txt = "Sua vez red";
    }
    if (!game_over) {
        win();
    }
}

function p(event) {
    f0 = 0;
    size = 50;
    clientX = Math.floor(event.pageX);
    clientY = Math.floor(event.pageY);
    // y
    grid1 = 55;
    grid2 = 116;
    grid3 = 177;
    // x
    grid4 = 115;
    grid5 = 176;
    grid6 = 237;
    // color object
    if (player === 1) {
        color = "blue";
    } else {
        color = "red";
    }
    if (game_over) {
        return;
    }
    if (clientX >= 115 && clientX <= 165 && clientY >= 55 && clientY <= 106 && !Objects.a1) {
        Objects.a1 = new Rect(size,size,color,grid4,grid1);
        f0 = 1;
    }
    if (clientX >= 176 && clientX <= 226 && clientY >= 55 && clientY <= 106 && !Objects.a2) {
        Objects.a2 = new Rect(size,size,color,grid5,grid1);
        f0 = 1;
    }
    if (clientX >= 237 && clientX <= 287 && clientY >= 55 && clientY <= 106 && !Objects.a3) {
        Objects.a3 = new Rect(size,size,color,grid6,grid1);
        f0 = 1;
    }
    if (clientX >= 115 && clientX <= 165 && clientY >= 116 && clientY <= 167 && !Objects.b1) {
        Objects.b1 = new Rect(size,size,color,grid4,grid2);
        f0 = 1;
    }
    if (clientX >= 176 && clientX <= 226 && clientY >= 116 && clientY <= 167 && !Objects.b2) {
        Objects.b2 = new Rect(size,size,color,grid5,grid2);
        f0 = 1;
    }
    if (clientX >= 237 && clientX <= 287 && clientY >= 116 && clientY <= 167 && !Objects.b3) {
        Objects.b3 = new Rect(size,size,color,grid6,grid2);
        f0 = 1;
    }
    if (clientX >= 115 && clientX <= 165 && clientY >= 177 && clientY <= 228 && !Objects.c1) {
        Objects.c1 = new Rect(size,size,color,grid4,grid3);
        f0 = 1;
    }
    if (clientX >= 176 && clientX <= 226 && clientY >= 177 && clientY <= 228 && !Objects.c2) {
        Objects.c2 = new Rect(size,size,color,grid5,grid3);
        f0 = 1;
    }
    if (clientX >= 237 && clientX <= 287 && clientY >= 177 && clientY <= 228 && !Objects.c3) {
        Objects.c3 = new Rect(size,size,color,grid6,grid3);
        f0 = 1;
    }
    if (f0 == 1 && player == 1) {
        player = 2;
    } else if (f0 == 1) {
        player = 1;
    }
}

function win() {
    p_ = Objects.myText;
    if (Objects.a1 && Objects.a2 && Objects.a3) {
        if (Objects.a1.color == Objects.a2.color && Objects.a3.color == Objects.a1.color) {
            game_over = true;
            p_.txt = "Player "+Objects.a1.color+" ganhou!";
            return;
        }
    }
    if (Objects.b1 && Objects.b2 && Objects.b3) {
        if (Objects.b1.color == Objects.b2.color && Objects.b1.color == Objects.b3.color) {
            game_over = true;
            p_.txt = "Player "+Objects.b1.color+" ganhou!";
            return;
        }
    }
    if (Objects.c1 && Objects.c2 && Objects.c3) {
        if (Objects.c1.color == Objects.c2.color && Objects.c1.color == Objects.c3.color) {
            game_over = true;
            p_.txt = "Player "+Objects.c1.color+" ganhou!";
            return;
        }
    }
    if (Objects.a1 && Objects.b1 && Objects.c1) {
        if (Objects.a1.color == Objects.b1.color && Objects.a1.color == Objects.c1.color) {
            game_over = true;
            p_.txt = "Player "+Objects.a1.color+" ganhou!";
            return;
        }
    }
    if (Objects.a2 && Objects.b2 && Objects.c2) {
        if (Objects.a2.color == Objects.b2.color && Objects.a2.color == Objects.c2.color) {
            game_over = true;
            p_.txt = "Player "+Objects.a2.color+" ganhou!";
            return;
        }
    }
    if (Objects.a3 && Objects.b3 && Objects.c3) {
        if (Objects.a3.color == Objects.b3.color && Objects.a3.color == Objects.c3.color) {
            game_over = true;
            p_.txt = "Player "+Objects.a3.color+" ganhou!";
            return;
        }
    }
    if (Objects.a1 && Objects.b2 && Objects.c3) {
        if (Objects.a1.color == Objects.b2.color && Objects.a1.color == Objects.c3.color) {
            game_over = true;
            p_.txt = "Player "+Objects.a1.color+" ganhou!";
            return;
        }
    }
    if (Objects.a3 && Objects.b2 && Objects.c1) {
        if (Objects.a3.color == Objects.b2.color && Objects.a3.color == Objects.c1.color) {
            game_over = true;
            p_.txt = "Player "+Objects.a3.color+" ganhou!";
            return;
        }
    }
    if (Objects.a1 && Objects.a2 && Objects.a3 && Objects.b1 && Objects.b2 && Objects.b3 && Objects.c1 && Objects.c2 && Objects.c3) {
        game_over = true;
        p_.txt = "Sem vencedor";
        return;
    }
}

function reset() {
    reset_game = true;
}