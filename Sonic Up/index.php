<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            canvas {
                border: 1px solid #d3d3d3;
                background-color: #f1f1f1;
            }
        </style>
    </head>
    <body onload="startGame()">
        <script src="Extension/runtime_extension.js"></script>
        <script src="runtime_events.js"></script>
        <script src="code.js"></script>
        <script src="objects.js"></script>
        <script src="runtimescene.js"></script>
        <script src="https://cdn.statically.io/gl/maurygta2/mquery/master/Rank Tools/rank.js"></script>
        <script src="Extension/isMobile.js"></script>
    </body>
</html>
