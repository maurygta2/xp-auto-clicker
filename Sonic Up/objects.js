// objects
const Objects = {
    Rect: function(width, height, color, x, y) {
        this.width = width;
        this.height = height;
        this.color = color;
        this.x = x;
        this.y = y;
        this.type = "Rect";
        this.update = function() {
            ctx = myGameArea.context;
            ctx.fillStyle = this.color;
            ctx.fillRect(this.x, this.y, this.width, this.height);
        };
    },
    Text : function(font,color,txt,x,y) {
        this.font = font;
        this.color = color;
        this.txt = txt;
        this.x = x;
        this.y = y;
        this.type = "Text";
        this.update = function() {
            ctx = myGameArea.context;
            ctx.font = this.font;
            ctx.fillStyle = this.color;
            ctx.fillText(this.txt,this.x,this.y);
        };
    },
    Image : function(img,width,heigth,x,y) {
        this.img = new Image();
        this.img.src = img;
        this.x = x;
        this.y = y;
        this.width = width;
        this.heigth = heigth;
        this.type = "Image";
        this.update = function() {
            ctx = myGameArea.context;
            ctx.drawImage(this.img,this.x,this.y,this.width,this.heigth);
        };
    }
};