function Scene(name,object,variable) {
    this.name = (typeof name == "string") ? name : "";
    this.object = (typeof object == "object") ? object : {};
    this.variable = (typeof variable == "object") ? variable : {};
    this.update = function() {
        for (let f0 in this.object) {
            this.object[f0].update();
        }
        this.scene_start = false;
    };
    this.start = function() {
        scenes[0] = this.name;
        this.scene_start = true;
    };
    this.scene_start = true;
}