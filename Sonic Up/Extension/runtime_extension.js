const Extensions = {};
function Extension(param) {
    this.name = param.name;
    this.description = param.description;
    this.version = param.version;
    this.author = param.author;
    this.on = param.on;
}