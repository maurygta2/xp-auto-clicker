Extensions.isMobile = new Extension({
    name: "isMobile",
    description: "É mobile? retornar true ou false",
    version: "1.0.0",
    author: "Maury",
    on: function() {
        var f0 = navigator.userAgent.toLowerCase();
	    if (f0.search(/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i)!= -1 ) {
	        return true;
	    } else {
	        return false;
	    }
    }
});