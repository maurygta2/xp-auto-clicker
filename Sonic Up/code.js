/*jshint maxerr: 10000000 */
// Global variable

const Elements = {};
var variable = {
    score: 0,
    scores: {},
};
const scenes = ["Inicio"];

// scene start
function startGame() {
    scenes[1] = new Scene("Inicio",{
        background : new Objects.Image(("img/Background.jpg"),400,270,0,0),
        start_ : new Objects.Rect(200,30,"white",110,95),
        start : new Objects.Text("30px Calibri","blue","START GAME",120,120),
    },{});
    scenes[2] = new Scene("Game",{
        background : new Objects.Image(("img/Background2.jpg"),400,270,0,0),
        plano: new Objects.Rect(400,20,"white",0,250),
        player: new Objects.Image(("img/player.png"),30,30,0,220),
        random: new Objects.Image(("img/tijolo.jpg"),30,30,Math.floor(Math.random()*14)*30,310),
        random2: new Objects.Image(("img/tijolo.jpg"),30,30,Math.floor(Math.random()*14)*30,310),
        myText : new Objects.Text("20px Calibri","black","Score: "+variable.score,40,40),
        myText2: new Objects.Text("15px Arial Black",'black',"Best Score: 0",40,65)
    },{
        time: 1,
        num: 0
    });
    myGameArea.start();
}

var myGameArea = {
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.width = 400;
        this.canvas.height = 270;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        if (!Extensions.isMobile.on()) {
            document.addEventListener("keyup",Events.Keyboard.keyup.on);
        } else {
            this.canvas.addEventListener("click",Events.Mouse.on);
        }
        this.interval = setInterval(updateGameArea, 20);
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
};

function updateGameArea() {
    myGameArea.clear();
    var game_scene;
    for (f0=1;f0<scenes.length;f0++) {
        if (scenes[f0].name == scenes[0]) {
            game_scene = scenes[f0];
            break;
        }
    }
    var game_object = (param) => {
        return game_scene.object[param];
    };
    var game_event = (param,param2,param3) => {
        if (param2 === undefined) {
            return Events[param];
        } else if (param3 === undefined) {
            return Events[param][param2];
        } else {
            return Events[param][param2][param3];
        }
    };
    if (scenes[0] == "Game") {
        if (game_scene.variable.time >= 0 && (game_event("Keyboard","keyup","d") || (game_event("Mouse","click") && game_event('Mouse',"x") > 200 && game_event("Mouse","y") > 180)) && game_object("player").x < 385) {
            game_object("player").x += 15;
            game_object("player").img.src = "img/player.png";
        }
        if (game_scene.variable.time >= 0 && (game_event("Keyboard","keyup","a") || (game_event("Mouse","click") && game_event("Mouse","x") < 200 && game_event("Mouse","y") > 180)) && game_object("player").x > 0) {
            game_object("player").x -= 15;
            game_object("player").img.src = "img/player2.png";
        }
        if (game_scene.variable.time >= 0 && (game_event("Keyboard","keyup","w") || (game_event("Mouse","click") && game_event("Mouse","y") < 180)) && Math.floor(game_object("player").y) == 220) {
            game_object("player").y -= 30;
        } else if (game_scene.variable.time >= 0 && game_object("player").y < 220) {
            game_object("player").y += 30/50;
        }
        if (game_object("player").y > 220) {
            game_object("player").y = 220;
        }
        if (game_scene.variable.time >= 0 && ((game_object("random2").y == 220 && game_object("random2").x > game_object("player").x -30 && game_object("random2").x <  game_object("player").x +30) || (game_object("random").y == 220 && game_object("random").x > game_object("player").x -30 && game_object("random").x < game_object("player").x +30))) {
            game_scene.variable.num += 1;
            variable.scores["p"+game_scene.variable.num] = variable.score;
            if (typeof Rank_Tools !== "undefined") {
                game_object("myText2").txt = "Best Score: "+Rank_Tools.rank(variable.scores,(pos,name,value) => {
                    return value;
                }).rank1;
            }
            game_scene.variable.time = -50;
        }
        if (game_scene.variable.time == -1) {
            variable.score = 0;
            game_object("player").x = 0;
            game_object("player").y = 220;
            game_object("random").y = 310;
            game_object("random2").y = 310;
        }
        if (game_scene.variable.time == 300) {
            game_object("random").x = Math.floor(Math.random()*14)*30;
            game_object("random").y = 220;
            game_object("random2").x = Math.floor(Math.random()*14)*30;
            game_object("random2").y = 220;
        }
        if (game_scene.variable.time >= 450) {
            game_object("random").y = 310;
            game_object("random2").y = 310;
            game_scene.variable.time = 0;
            variable.score += 1;
        }
        game_object("myText").txt = "Score: "+variable.score;
        game_scene.variable.time += 1;
        game_scene.update();
    }
    if (scenes[0] == "Inicio") {
        if (game_event("Mouse","click") && game_event("Mouse","x") > 110 && game_event("Mouse","x") < 310 && game_event("Mouse","y") > 95 && game_event("Mouse","y") < 125) {
            scenes[2].start();
        }
        game_scene.update();
    }
    Events.update();
}