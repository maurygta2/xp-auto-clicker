const Events = {
    Mouse : {
        click : false,
        x : null,
        y : null,
        on : (event) => {
            Events.Mouse.click = true;
            Events.Mouse.x = event.pageX;
            Events.Mouse.y = event.pageY;
        }
    },
    Keyboard : {
        keyup: {
            "a" : false,
            "d" : false,
            "w" : false,
            on : (event) => {
                if (Events.Keyboard.keyup[event.key] !== undefined) {
                    Events.Keyboard.keyup[event.key] = true;
                }
            }
        }
    },
    update : () => {
        Events.Mouse.click = false;
        for (let f0 in Events.Keyboard.keyup) {
            if (Events.Keyboard.keyup[f0] === true) {
                Events.Keyboard.keyup[f0] = false;
            }
        }
    }
};